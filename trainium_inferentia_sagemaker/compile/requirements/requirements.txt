--extra-index-url=https://pip.repos.neuron.amazonaws.com
torch==1.10.2
torch-neuron==1.10.2.*
neuron-cc[tensorflow]
transformers<4.20.0
numpy==1.19.5
protobuf==3.20.1
